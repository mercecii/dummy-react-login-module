import logo from './logo.svg';
import './App.css';
// import {DummyButton, DummyInput} from 'dummy-react-npm-module';
import * as DummyComponents from 'dummy-react-npm-module';

import * as ErrorModule from 'dummy-react-error-module';


// import {ErrorNotice } from 'dummy-react-error-module/notice'

console.log('DummyComponents = ', DummyComponents);
console.log('ErrorModule = ', ErrorModule);

const {DummyButton, DummyInput} = DummyComponents;
const {ErrorWrapper} = ErrorModule;

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Parent is Login Module
        </p>
        <DummyButton />
        <DummyInput />
        <ErrorWrapper 
          text="This is Error text 1 for Warning"
          errorType="warning"
        />
        <ErrorWrapper 
          text="This is Error text 1 for Error"
          errorType="critical"
        />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
